import {FC} from 'react';
import styles from './projects.module.css';
import {useQuery} from 'react-query';
import {fetchProjects} from 'domain/api/projects';
import Link from 'next/link';

export const Projects: FC = () => {
  const { data: projects, isFetching } = useQuery(['project', 'list'], () => fetchProjects(), { suspense: true });

  if (!projects) {
    return null;
  }

  return (
    <>
      {isFetching && 'Refetching...'}
      <ul className={styles.projects}>
      {projects.map((projectSummary) => (
        <li
          key={projectSummary.id}
        >
          <Link className={styles.project} href={`/project/${projectSummary.id}`}>
            <div>id: {projectSummary.id}</div>
            <div>name: {projectSummary.name}</div>
            <div>manager: {projectSummary.manager}</div>
            <div>total #: {projectSummary.total}</div>
            <div>volume: {projectSummary.totalVolume}</div>
            <div>previews:
              [{projectSummary.previews.join(', ')}{projectSummary.previews.length < projectSummary.total ? '...' : ''}]
            </div>
          </Link>
        </li>
      ))}
    </ul>
  </>
  );
};
