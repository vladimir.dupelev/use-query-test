import {FC} from 'react';
import styles from './projectSummary.module.css';
import {useIsMutating, useQuery} from 'react-query';
import {fetchProjectSummary} from 'domain/api/projects';

interface Props {
  id: string;
}

export const ProjectSummary: FC<Props> = ({id}) => {
  const {
    data: projectSummary,
    isFetching
  } = useQuery(['project', 'summary', id], () => fetchProjectSummary(id), {suspense: true});
  const isMutating = useIsMutating(['project']) > 0 || isFetching;

  if (!projectSummary) {
    return null;
  }

  return (
    <div className={styles.project}>
      <div>id: {projectSummary.id}</div>
      <div>name: {projectSummary.name}</div>
      <div>manager: {projectSummary.manager}</div>
      <div>total #: {projectSummary.total}{isMutating && '?'}</div>
      <div>volume: {projectSummary.totalVolume}{isMutating && '?'}</div>
      <div>previews:
        [{projectSummary.previews.join(', ')}{projectSummary.previews.length < projectSummary.total ? '...' : ''}]
        {isMutating && '?'}
      </div>
    </div>
  );
};
