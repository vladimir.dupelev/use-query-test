import {FC, useState} from 'react';
import styles from './keywords.module.css';
import {useMutation, useQuery, useQueryClient} from 'react-query';
import {addKeyword, fetchKeywords} from 'domain/api/keywords';
import {KeywordItem} from 'components/project/keywordItem';
import {Keyword} from 'domain/models/keyword';

interface Props {
  projectId: string;
}

export const Keywords: FC<Props> = ({projectId}) => {
  const queryClient = useQueryClient();
  const {data: keywords} = useQuery(['project', 'keywords', projectId], () => fetchKeywords(projectId), {suspense: true});

  const addKeywordMutation = useMutation((newKeyword: string) => addKeyword(projectId, newKeyword), {
    mutationKey: 'project',
    onMutate: (newKeyword) => {
      const newObject = {phrase: newKeyword, volume: NaN, projectId};
      queryClient.setQueryData<Keyword[] | undefined>(['project', 'keywords', projectId], (oldData) =>
        oldData ? [...oldData, newObject] : [newObject]
      );
    },
    onSuccess: () => {
      void queryClient.invalidateQueries('project');
    }
  });

  const [newPhrase, setNewPhrase] = useState<string>('');

  const handleAddKeyword = async () => {
    await addKeywordMutation.mutateAsync(newPhrase);
    setNewPhrase('');
  };

  if (!keywords) {
    return null;
  }

  return (
    <ul className={styles.keywords}>
      {keywords.map(keyword => (
        <KeywordItem key={keyword.phrase} projectId={projectId} keyword={keyword} />
      ))}
      <li key='add' className={styles.keyword}>
        {addKeywordMutation.isLoading ? (
          <span>Adding...</span>
        ) : (
          <>
            <input
              type='text'
              value={newPhrase}
              onChange={(e) => setNewPhrase(e.target.value)}
            />
            <button onClick={handleAddKeyword}>Add Keyword</button>
          </>
        )}
      </li>
    </ul>
  );
};
