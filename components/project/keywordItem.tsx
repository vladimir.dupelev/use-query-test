import {FC} from 'react';
import styles from './keywords.module.css';
import {useMutation, useQueryClient} from 'react-query';
import {removeKeyword} from 'domain/api/keywords';
import {Keyword} from 'domain/models/keyword';

interface Props {
  projectId: string;
  keyword: Keyword;
}

export const KeywordItem: FC<Props> = ({projectId, keyword}) => {
  const queryClient = useQueryClient();
  const removeKeywordMutation = useMutation((keywordId: string) => removeKeyword(projectId, keywordId), {
    mutationKey: 'project',
    onMutate: () => {
      queryClient.setQueryData<Keyword[] | undefined>(['project', 'keywords', projectId], (oldData) =>
        oldData?.filter((k) => k.phrase !== keyword.phrase)
      );
    },
    onSuccess: () => {
      void queryClient.invalidateQueries('project');
    }
  });

  const handleDelete = async (keyword: Keyword) => {
    await removeKeywordMutation.mutateAsync(keyword.phrase);
  };

  return (
    <li key={keyword.phrase} className={styles.keyword}>
      <div>phrase: {keyword.phrase}</div>
      <div>volume: {keyword.volume}</div>
      <div>
        {
          removeKeywordMutation.isLoading ? <span>Deleting...</span> :
            <button onClick={() => handleDelete(keyword)}>Delete</button>
        }
      </div>
    </li>
  );
};
