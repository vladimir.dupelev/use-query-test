import {useEffect, useState} from 'react'


export function isClient() {
  return typeof window !== 'undefined';
}

export function useIsClient() {
  const [isClient, setClient] = useState(false)

  useEffect(() => {
    setClient(true)
  }, [])

  return isClient;
}
