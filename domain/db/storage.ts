import {Project} from 'domain/models/project';
import {Keyword} from 'domain/models/keyword';

export const initProjectsData = [
  {
    id: 'p1',
    name: 'Project #1',
    manager: 'Vladimir'
  },
  {
    id: 'p2',
    name: 'Project #2',
    manager: 'Dima'
  },
  {
    id: 'p3',
    name: 'Project #3',
    manager: 'Vera'
  },
  {
    id: 'p4',
    name: 'Project #4',
    manager: 'Unassigned'
  }
];

export const initKeywordsData: Record<Project['id'], Keyword[]> = {
  'p1': [
    {projectId: 'p1', phrase: 'k101', volume: 200},
    {projectId: 'p1', phrase: 'k102', volume: 20},
    {projectId: 'p1', phrase: 'k103', volume: 12},
    {projectId: 'p1', phrase: 'k104', volume: 2}
  ],
  'p2': [
    {projectId: 'p2', phrase: 'k201', volume: 400},
    {projectId: 'p2', phrase: 'k202', volume: 44},
    {projectId: 'p2', phrase: 'k203', volume: 14},
    {projectId: 'p2', phrase: 'k204', volume: 4}
  ],
  'p3': [
    {projectId: 'p3', phrase: 'k301', volume: 800},
    {projectId: 'p3', phrase: 'k302', volume: 88}
  ],
  'p4': []
};

interface IStorage {
  projectsData: typeof initProjectsData,
  keywordsData: typeof initKeywordsData
}

let __storage: IStorage | null = null;

function getStorage(): IStorage {
  if (!__storage) {
    console.warn('!!!init storage');
    __storage = {
      projectsData: initProjectsData,
      keywordsData: initKeywordsData
    }
  }

  return __storage;
}

// Get the current storage data
export function getKeywords(projectId: string) {
  return getStorage().keywordsData[projectId];
}

// Get the current storage data
export function getProjects() {
  return getStorage().projectsData;
}

// Get the current storage data
export function getProject(projectId: string) {
  return getStorage().projectsData.find(p => p.id === projectId);
}

// Add a keyword to the storage
export function addKeyword(projectId: string, keyword: Keyword) {
  const keywordId = Date.now();
  getStorage().keywordsData[projectId].push(keyword);
}

// Remove a keyword from the storage
export function removeKeyword(projectId: string, phrase: string) {
  const index = getStorage().keywordsData[projectId].findIndex((k) => k.phrase === phrase);
  getStorage().keywordsData[projectId].splice(index, 1);
}
