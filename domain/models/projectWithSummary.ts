import {Project} from 'domain/models/project';

export interface ProjectWithSummary extends Project{
  previews: string[],
  total: number,
  totalVolume: number
}
