export interface Project {
  id: string,
  name: string,
  manager: string
}
