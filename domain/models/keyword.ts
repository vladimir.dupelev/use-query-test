import {Project} from './project';

export interface Keyword {
  phrase: string,
  volume: number,
  projectId: Project['id']
}
