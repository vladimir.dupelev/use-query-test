import axios from 'axios';
import {ProjectWithSummary} from 'domain/models/projectWithSummary';

export async function fetchProjects() {
  const {data} = await axios.get(`/api/projects`);
  return data as ProjectWithSummary[];
}

export async function fetchProjectSummary(id: string) {
  const {data} = await axios.get(`/api/projectSummary?id=${id}`);
  return data as ProjectWithSummary;
}
