import axios from 'axios';
import {Keyword} from 'domain/models/keyword';

export async function fetchKeywords(projectId: string) {
  const {data} = await axios.get(`/api/keywords?projectId=${projectId}`);
  return data as Keyword[];
}

export async function  addKeyword (projectId: string, phrase: string) {
  const {data} = await axios.get(`/api/keywords/add?projectId=${projectId}&phrase=${phrase}`);
}

export async function  removeKeyword (projectId: string, phrase: string) {
  await axios.get(`/api/keywords/remove?projectId=${projectId}&phrase=${phrase}`);
}
