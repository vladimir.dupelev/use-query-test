import type {NextPage} from 'next';
import {Suspense} from 'react';
import {Projects} from 'components/projects';
import {useIsClient} from 'utils/isClient';

const Home: NextPage = () => {
  const isClient = useIsClient();

  function FullSpinner() {
    return (
      <div className='full-spinner'>
        <p>loading....</p>
      </div>
    );
  }

  if (!isClient) {
    return null;
  }

  return <>
    Projects:
    <Suspense fallback={<FullSpinner />}>
      <Projects />
    </Suspense>
  </>;
};

export default Home;
