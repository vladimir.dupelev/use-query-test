import type {NextApiRequest, NextApiResponse} from 'next';
import {Keyword} from 'domain/models/keyword';
import {delay} from 'utils/delay';
import {removeKeyword, getKeywords} from 'domain/db/storage';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Keyword[] | string>
) {
  await delay(1000);

  const requestedProjectId = req.query.projectId as string;
  const phrase = req.query.phrase as string;

  if (!requestedProjectId) {
    res.status(400).send('project id required');
    return;
  }

  if (!phrase) {
    res.status(400).send('phrase id required');
    return;
  }

  const keywords = getKeywords(requestedProjectId);

  if (!keywords) {
    res.status(400).send('project id should be from the allowed list');
    return;
  }

  if (!keywords.find( (k) => k.phrase === phrase)) {
    res.status(400).send('keyword should be in the list');
    return;
  }

  removeKeyword(requestedProjectId, phrase);

  res.status(200).send('ok');
}
