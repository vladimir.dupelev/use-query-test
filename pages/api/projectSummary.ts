import type {NextApiRequest, NextApiResponse} from 'next';
import {delay} from 'utils/delay';
import {getKeywords, getProject} from 'domain/db/storage';
import {ProjectWithSummary} from 'domain/models/projectWithSummary';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ProjectWithSummary | string>
) {
  await delay(1000);
  const projectId = req.query.id as string;

  if (!projectId) {
    res.status(400).send('project id required');
    return;
  }

  const project = getProject(projectId);

  if (!project) {
    res.status(400).send('invalid project id');
    return;
  }

  const keywords = getKeywords(projectId);

  const projectWithTaskSummary = {
    ...project,
    total: keywords.length,
    previews: keywords.slice(0, 2).map(keyword => keyword.phrase),
    totalVolume: keywords.reduce((a, keyword) => a + keyword.volume, 0)
  };

  res.status(200).json(projectWithTaskSummary);
}
