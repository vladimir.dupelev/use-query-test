import type {NextApiRequest, NextApiResponse} from 'next';
import {delay} from 'utils/delay';
import {ProjectWithSummary} from 'domain/models/projectWithSummary';
import {getKeywords, getProjects} from 'domain/db/storage';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ProjectWithSummary[]>
) {
  await delay(1000);

  const projectsWithTaskSummary = getProjects().map(project => {
    const keywords = getKeywords(project.id);
    return {
      ...project,
      total: keywords.length,
      previews: keywords.slice(0, 2).map(keyword => keyword.phrase),
      totalVolume: keywords.reduce((a, keyword) => a + keyword.volume , 0)
    }
  });

  res.status(200).json(projectsWithTaskSummary);
}
