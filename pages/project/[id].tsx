import type {NextPage} from 'next';
import {Suspense} from 'react';
import {useIsClient} from 'utils/isClient';
import {useRouter} from 'next/router';
import {ProjectSummary} from 'components/project/projectSummary';
import {Keywords} from 'components/project/keywords';
import styles from '../../styles/Home.module.css';
import Link from 'next/link';

const Project: NextPage = () => {
  const isClient = useIsClient();
  const router = useRouter();
  const id = router.query.id as string;

  function FullSpinner() {
    return (
      <div className='full-spinner'>
        <p>loading....</p>
      </div>
    );
  }

  if (!isClient) {
    return null;
  }

  if (!id) {
    return <>Wrong project!</>;
  }

  return <>
    <nav>
      <Link href={'/'}>Back to the Projects</Link>
    </nav>
    <section className={styles.section}>
      Project [{id}] Summary:
      <Suspense fallback={<FullSpinner />}>
        <ProjectSummary id={id} />
      </Suspense>
    </section>
    <section className={styles.section}>
      Tasks:
      <Suspense fallback={<FullSpinner />}>
        <Keywords projectId={id} />
      </Suspense>
    </section>
  </>;
};

export default Project;
